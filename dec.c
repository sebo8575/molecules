#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <getopt.h>
#include <math.h>
#include <ctype.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/types.h>


#define limC     3000
#define limH    30000
#define limO      400
#define limN      200
#define limS      120
#define limP      100
#define limCl      10
// Check if product of all these numbers fit in uint64

#define ZEILENLAENGE    1024
#define MAX_MASS        2000
#define MAX_SEARCH   5000000
#define MAX_ELE           32
#define MAX_MASSES   5000000
#define MAX_RESULTS   500000
#define MAX_SPECS          2 // how many special elements allowed
#define TOLMUL         1.0E7
#define TOLDIV     10000000LL

void print_results();
void dump_aF(char* buf, int* aF);
void print_table_header();


int DO_DEBUG=0;

typedef struct {
    uint64_t orgsearch;
    uint64_t diff;
    int a[MAX_ELE];
    int idx;
} Result;
Result results[MAX_RESULTS];
int last_result = 0;

typedef struct {
    uint64_t  mass;
    uint64_t  formel;
} Moli;


typedef struct {
    int64_t last; // list length
    Moli* a;      // list of moli tuples
} Moli1;
Moli1*  mols[MAX_MASS]; // array of lists to store mass and formula tuples

// both just for reading textfiles
Moli*   mols0;
int64_t mols0_last = 0;


typedef struct {
    uint64_t  mass;
    uint64_t  searchmass;
    int idx;
    int a[MAX_ELE];
} Masses;
Masses masses[MAX_MASSES];
uint64_t last_masses = 0;

typedef struct {
   char    formel[10];
   uint64_t      mass;
   int        valence;
   int          spec0;
} Element;

// struct for elements describing loop and other props
typedef struct {
    char    formel[10];  // Just String for formula, C13
    int     cFrom;       // Min number of elements in resuilt
    int     cTo;         // Max Number in result
    char*   ref;         // Refered Element for replacements (c13 <-> C)
    int     iref;        // index of refered Element (calculated)
    int64_t delta;       // mass difference if added or replaced, so mass for added elements, C13-C for replacements
    Element e;           // reference to element
} Count;

Element elements[] = {
    { "C"    ,     12000000000  ,  4, 0 },    // 0
    { "H"    ,      1007825000  ,  1, 0 },    // 1
    { "O"    ,     15994914619  ,  2, 0 },    // 2
    { "N"    ,     14003074004  ,  5, 0 },    // 3
    { "S"    ,     31972072000  ,  6, 0 },    // 4
    { "P"    ,     30973763000  ,  5, 0 },    // 5
    { "Cl"   ,     34968852700  ,  1, 1 },    // 6
    { "Na"   ,     22989769280  ,  1, 1 },    // 7
    { "C13"  ,     13003354835  ,  4, 2 },    // 8
    { "O18"  ,     17999161000  ,  2, 2 },    // 9
    { "N15"  ,     15000108899  ,  5, 2 },    // 10
    { "S34"  ,     33967866900  ,  6, 2 },    // 11
    { "Cl37" ,     36965902590  ,  1, 1 },    // 11
    { "Co"   ,     58933195000  ,  2, 1 },    // 12
    { "Cu.i"  ,    62929597720  ,  1, 1 },    // 13
    { "Cu.ii" ,    62929597720  ,  2, 1 },    // 14
    { "Cu65.i" ,   64927789500  ,  1, 1 },    // 15
    { "Cu65.ii",   64927789500  ,  2, 1 },    // 16
    { "Fe.ii" ,    55934936000  ,  2, 1 },    // 17
    { "Fe.iii",    55934936000  ,  3, 1 },      // 18
    { "Fe54.ii" ,  53939609000  ,  2, 1 },    // 19
    { "Fe54.iii",  53939609000  ,  3, 1 },    // 20
    { "Br79" ,     78918338000  ,  1, 1 },       // 21
    { "Br81" ,     80916900000  ,  1, 1 },       // 22
    { "Ni.ii" ,    57935346200  ,  2, 1 },    // 23
    { "Ni60.ii" ,  59930786400  ,  2, 1 },    // 24
    { "Zn.ii" ,    65380000000  ,  2, 1 },    // 25
    { "Zn66.ii" ,  65926033400  ,  2, 1 },    // 26
    { "I"    ,    126904470000  ,  1, 1 } };     // 27
int NBR_ELEMENTS= sizeof(elements)/sizeof(Element);

#define COUNTS_OFFSET 7 // index of first not pregenerated element

// All the counts with min and max for the additinal elements, missing cols added in init_alternatives
Count mol_counts[] = {
    { "Na"     ,   0,   2, NULL },
    { "C13"    ,   0,   3, "C" },
    { "O18"    ,   0,   1, "O" },
    { "N15"    ,   0,   1, "N" },
    { "S34"    ,   0,   1, "S" },
    { "Cl37"    ,  0,   1, "Cl" },
    { "Co"      ,  0,   2, NULL },
    { "Cu.i"    ,  0,   2, NULL },
    { "Cu.ii"   ,  0,   2, NULL },
    { "Cu65.i"    ,0,   2, "Cu.i" },
    { "Cu65.ii"   ,0,   2, "Cu.ii" },
    { "Fe.ii"   ,  0,   2, NULL },
    { "Fe.iii"  ,  0,   2, NULL },
    { "Fe54.ii"   ,0,   2, "Fe.ii" },
    { "Fe54.iii"  ,0,   2, "Fe.iii" },
    { "Br79"   ,   0,   2, NULL },
    { "Br81"   ,   0,   2, NULL },
    { "Ni.ii"   ,  0,   2, NULL },
    { "Ni60.ii"   ,0,   2, "Ni.ii" },
    { "Zn.ii"   ,  0,   2, NULL },
    { "Zn66.ii"   ,0,   2, "Zn.ii" },
    { "I"      ,   0,   2, NULL },
};
int NBR_COUNTS= sizeof(mol_counts)/sizeof(Count);
int cur_mol[MAX_ELE]; // array with current element numbers

int64_t searchweights[MAX_SEARCH];

void dump_aF(char* buf, int* aF) {
    buf[0]= '\0';
    for (int i=0; i<MAX_ELE; i++) {
        sprintf(buf+strlen(buf), "%2d ", aF[i]);
    }
}


double calc_hc(int* aF) {
    return (double)aF[1] / (double)(aF[0]+aF[8]);
}

double calc_oc(int* aF) {
    return (double)aF[2] / (double)(aF[0]+aF[8]);
}

/* Takes an Element in aE and number of apperance in card
 * and stores it in the respective position in int array aF */
void storeElement(int* aF, char* sE, int card) {
    if (strlen(sE)==0) {
        return;
    }

    for (int i=0; i<NBR_ELEMENTS; i++) {
        if ( ! strcmp(sE, elements[i].formel)) {
            aF[i]= card;
        }
    }

}


/* Takes a formel- string in the form "C_16 H_32 BR79_2",
 * so its Element, underscore Abundance Space or \0
 * and fills array aF */
void formel2aF(int* aF, char* f) {
    char  sE[MAX_ELE] = "";
    char* pE = sE;
    int i=0;
    int card=0;
    int state = 0;
    int len = strlen(f);
    for (int i=0; i < MAX_ELE; i++) {
        aF[i]= 0;
    }

    while (f[i]!='\0' && i<len) {
        if ((state == 0 && isalpha(f[i]) || state == 1) && f[i] != '_'){
            state = 1;
            *pE = f[i++];
            pE++;
            *pE = '\0';
        } else if (f[i] == '_') {
            state=2;
            i += 1;
        } else if (state == 2 && f[i]>='0' && f[i]<='9') {
            card=card*10 + f[i++] - 48;
        } else if (f[i] == ' ' || f[i] == '\0' || f[i] == '\n') {
            storeElement(aF, sE, card);
            i += 1;
            state = 0;
            sE[0]= '\0';
            card=0;
            pE= sE;
        } else {
            printf("Some error in %s", f);
            exit(9);
        }
    }
    storeElement(aF, sE, card);
}


char* aF2list(char* f, int* aF) {
    f[0]= '\0';
    for (int i=0; i<NBR_ELEMENTS; i++) {
        sprintf(f+strlen(f), "%d\t", aF[i]);
    }
    f[strlen(f)-1]='\0';
    return f;
}


char* aF2formel(char* f, int* aF) {

    f[0]= '\0';
    for (int i=0; i<NBR_ELEMENTS; i++) {
        if (aF[i]) {
            sprintf(f+strlen(f), "%s_%d ", elements[i].formel, aF[i]);
        }
    }
    f[strlen(f)-1]='\0';
    return f;
}


void int2aF(int* aF, uint64_t iF) {
    int i;

    for (i=0; i < MAX_ELE; i++) {
        aF[i]= 0;
    }

    i=0;
    aF[i++]= iF % limC;
    iF = iF / limC;

    aF[i++]= iF % limH;
    iF = iF / limH;

    aF[i++]= iF % limO;
    iF = iF / limO;

    aF[i++]= iF % limN;
    iF = iF / limN;

    aF[i++]= iF % limS;
    iF = iF / limS;

    aF[i++]= iF % limP;
    iF = iF / limP;

    aF[i++]= iF % limCl;
    iF = iF / limCl;

}


uint64_t aF2int(int* aF) {
    uint64_t r = 0;
    uint64_t m = 1LL;
    int i = 0;

    r += (uint64_t)aF[i++] * m;
    m *= limC;

    r += (uint64_t)aF[i++] * m;
    m *= limH;

    r += (uint64_t)aF[i++] * m;
    m *= limO;

    r += (uint64_t)aF[i++] * m;
    m *= limN;

    r += (uint64_t)aF[i++] * m;
    m *= limS;

    r += (uint64_t)aF[i++] * m;
    m *= limP;

    r += (uint64_t)aF[i++] * m;
    m *= limCl;

    return r;
}


void mass2str(char* s, uint64_t m) {
    char r[256];
    sprintf(r, "%010ld", m);
    char* nk= r+(strlen(r)-9);
    char* p=r;
    char* q=s;
    while (*p) {
        *q=*p;
        p++;
        q++;
        if (p==nk) *q++= '.';
    }
    *q= '\0';
}

uint64_t mass_aF(int* aF) {
    uint64_t  m= 0;
    for (int i = 0; i < NBR_ELEMENTS; ++i) {
        if (aF[i]>0) {
            m = m + aF[i]*elements[i].mass;
        }
    }
    return m;
}


int val_aF(int* aF) {
    int  v= 0;
    for (int i = 0; i < NBR_ELEMENTS; ++i) {
        if (aF[i]>0) {
            v = v + aF[i]*elements[i].valence;
        }
    }
    return v;
}


uint64_t mass_mol() {
    uint64_t  m= 0;

    for (int i = 0; i < NBR_COUNTS; ++i) {
        if (cur_mol[i]>0) {
            m = m + cur_mol[i]*mol_counts[i].delta;
        }
    }
    return m;
}


char* usage() {
    return "USAGE: dec OPTIONS where options is one of: \n -r FILENAME1 [FILENAME2 ...] -o DESTFILE to read files with\n moleculelines, i.e.\n WEIGHT in MOL *10^9 Tab Formula as String C_13 H_45 ...\n and put result to outfile";
}


int push_result(int idx, uint64_t searchmass, uint64_t mass, int* aF) {
    if (last_result>=MAX_RESULTS) {
        print_results();
    }
    results[last_result].idx = idx;
    results[last_result].orgsearch = searchmass;
    if (mass>searchmass)
        results[last_result].diff = mass-searchmass;
    else
        results[last_result].diff = searchmass-mass;

    if (results[last_result].diff > 1000000) {
        fprintf(stderr, "Diff zu gross %ld (%ld, %ld)\n", results[last_result].diff, mass, searchmass);
        exit(9);
    }
    for (int i=0; i<NBR_ELEMENTS; i++)
        results[last_result].a[i]= aF[i];
    last_result+=1;
}

int push_mass(int idx, uint64_t searchmass, uint64_t mass) {
    masses[last_masses].idx= idx;
    masses[last_masses].searchmass= searchmass;
    masses[last_masses].mass= mass;
    for (int j=0; j<NBR_COUNTS; j++) {
        masses[last_masses].a[j]= cur_mol[j];
    }
    last_masses++;
    return last_masses>=MAX_MASSES;
}


void pull_mass(int i) {
    for (int j=0; j<NBR_COUNTS; j++) {
        cur_mol[j]=  masses[i].a[j];
    }
}


/** used for sorting */
int compareMasses( const void* a, const void* b)
{
     Masses ma = * ( (Masses*) a );
     Masses mb = * ( (Masses*) b );

     if ( ma.mass == mb.mass ) return 0;
     else if ( ma.mass < mb.mass ) return -1;
     else return 1;
}




void read_file(char* infilename) {
    FILE* fp;
    char line[ZEILENLAENGE];
    int64_t weight;
    int64_t iF;
    int aF[MAX_ELE];

    fprintf(stderr, "read %s\n",infilename);
    mols0 = (Moli*)malloc(sizeof(Moli) * 100000000);
    if( (fp=fopen(infilename,"r")) == NULL) {
        // fprintf(stderr, "Kann %s nicht oeffnen\n", infilename);
        exit(9);
    }
    while(fgets(line, ZEILENLAENGE, fp)) {
        // parse line
        char *p, *l, c;
        p=line;
        while (isspace((unsigned char)*p)) p++;
        l=p;
        while ('0'<=*l && *l<='9') l++;
        c= *l;
        *l='\0';
        weight= atoll(p);
        if (c=='\t') {
            p=l+1;
        } else {
            while (*p!='\t') p++;
            p += 1;
        }

        formel2aF(aF, p);
        iF= aF2int(aF);
        mols0[mols0_last++] = (Moli){ weight, iF };
    }
    fclose(fp);
}


Moli1* read_binfile(char* infilename) {
    FILE* fp;
    int64_t vers= 0x000000000001L;
    Moli1* m;
    uint64_t cnt;

    m= (Moli1*)malloc(sizeof(Moli1));

    if( (fp=fopen(infilename,"rb")) == NULL) {
        m->last= 0;
        m->a= NULL;
        return m;
    }

    /* fprintf(stderr, "read_file %s\n", infilename);
     */

    cnt = fread(&(m->last), sizeof(int64_t), 1, fp);
    if (cnt!=1) exit(2);
    cnt = fread(&vers, sizeof(int64_t), 1, fp);
    if (cnt!=1) exit(2);
    m->a= (Moli*)malloc(sizeof(Moli1) * m->last);
    cnt = fread(m->a, sizeof(Moli), m->last, fp);
    if (cnt!=m->last) {
        fprintf(stderr, "File %s, Expected %ld lines, found %ld\n",
            infilename, m->last, cnt );
        exit(2);
    }
    if (DO_DEBUG) fprintf(stderr, "    Read %s %ld\n", infilename, m->last);

    fclose(fp);
    return m;
}


int weight2int(int64_t weight) {
    int64_t fi = weight/1000000000LL;
    return (int)fi;
}


void read_filesfor(int idx, int64_t w,  int64_t tolerance) {
    char fn[256];
    int iweight;
    iweight = weight2int(w-tolerance);
    if (iweight<0 || iweight>2000) {
        fprintf(stderr, "Weight %ld out of range for %d\n", w, idx);
        exit(9);
    }
    if (iweight>5 && iweight<2000) {
        for (int i=0; i<iweight-5; i++) {
            if (mols[i] && mols[i]->last) {
                if (DO_DEBUG) fprintf(stderr, "    Free   %4d %ld\n", i, mols[i]->last);
                free(mols[i]->a);
                free(mols[i]);
                mols[i]= NULL;
            }
        }
    }
    if (iweight>0 && iweight<2000) {
        sprintf(fn, "weights/w%04d.bin", iweight);
        if (!mols[iweight]) {
            if (DO_DEBUG) fprintf(stderr, "    Alloc  %4d\n", iweight);
            mols[iweight]= read_binfile(fn);
        }
    }
    if (iweight>0 && iweight<2000) {
        iweight = weight2int(w+tolerance);
        sprintf(fn, "weights/w%04d.bin", iweight);
        if (!mols[iweight]) {
            if (DO_DEBUG) fprintf(stderr, "    Alloc2 %4d\n", iweight);
            mols[iweight]= read_binfile(fn);
        }
    }
}


void write_binfile(char* outfilename) {
    FILE* fp;
    int64_t vers= 0x000000000001L;
    if( (fp=fopen(outfilename,"wb")) == NULL) {
        fprintf(stderr, "Kann %s nicht oeffnen\n", outfilename);
        exit(9);
    }
    printf("write %ld lines\n", mols0_last);
    fwrite(&mols0_last, sizeof(int64_t), 1, fp);
    fwrite(&vers, sizeof(int64_t), 1, fp);
    fwrite(mols0, sizeof(Moli), mols0_last, fp);
    fclose(fp);
}


int file_exist (char *filename) {
    struct stat   sb;
    return (stat (filename, &sb) == 0 && S_ISREG(sb.st_mode));
}


void print_table_header() {
    char molsnames[256];
    molsnames[0]=0;
    for (int i=0; i<NBR_ELEMENTS; i++)
        sprintf(molsnames+strlen(molsnames),"%s\t", elements[i].formel);
    *(molsnames+strlen(molsnames)-1)= '\0';
    printf("ID\tvalueData\tvalueMaster\tdiff     \tH.C      \tO.C      \t%s\tformula\n",
        molsnames);

}


int compareResults( const void* a, const void* b)
{
     Result ma = * ( (Result*) a );
     Result mb = * ( (Result*) b );

     if ( ma.orgsearch == mb.orgsearch) {
        if (ma.diff == mb.diff) return 0;
        else if ( ma.diff < mb.diff ) return -1;
        else return 1;
     }
     else if ( ma.orgsearch < mb.orgsearch ) return -1;
     else return 1;
}

int compareMoli( const void* a, const void* b)
{
     Moli ma = * ( (Moli*) a );
     Moli mb = * ( (Moli*) b );

     if ( ma.mass == mb.mass ) return 0;
     else if ( ma.mass < mb.mass ) return -1;
     else return 1;
}


int count_mol() {
    int nbr = 0;

    for (int i = 0; i < NBR_COUNTS; ++i)
        nbr+= cur_mol[i];
    return nbr;
}

int next_mol() {
    char str[256];
    int nbr = 0;

    nbr=count_mol();

    for (int i = 0; i < NBR_COUNTS; ++i) {
        if (cur_mol[i] < mol_counts[i].cTo && nbr < MAX_SPECS) {
            cur_mol[i] += 1;
            return 1;
        } else {
            cur_mol[i] = mol_counts[i].cFrom;
            nbr=count_mol();
        }
    }
    return 0;
}


void init_alternatives() {
    // initialize start mol and copy elements
    for (int i = 0; i < NBR_COUNTS; ++i) {
        cur_mol[i]= mol_counts[i].cFrom;  // init loop
        for (int j = 0; j < NBR_ELEMENTS; ++j) {
            // Fill missing values in mol_counts
            if (strcmp(mol_counts[i].formel, elements[j].formel) == 0) {
                memcpy(&(mol_counts[i].e), &(elements[j]), sizeof(Element));
                mol_counts[i].e= elements[j];
                mol_counts[i].delta= mol_counts[i].e.mass;
                break;
            }
        }

        if (mol_counts[i].ref) {
            // Still fill missing values in mol_counts, in case of ref modify delta and enter index of refed element
            for (int j = 0; j < NBR_ELEMENTS; ++j) {
                if (strcmp(mol_counts[i].ref, elements[j].formel) == 0) {
                    mol_counts[i].delta= mol_counts[i].e.mass - elements[j].mass;
                    mol_counts[i].iref = j;
                }
            }
        }
    }
}


int check_mol(int* aF) {
    // check number of special elements
    int sum= 0;
    for (int i=COUNTS_OFFSET; i<NBR_ELEMENTS; i++) {
        sum+= aF[i];
    }
    if (sum>MAX_SPECS) return 0;

    // check valence
    for (int i=0; i<NBR_ELEMENTS; i++) {
        if (aF[i]<0) return 0;
    }
    if (val_aF(aF) % 2 == 1) return 0;
    return 1;
}

uint64_t search_moli1(uint64_t target, Moli1* m, uint64_t first, uint64_t last) {
    int test;
    if (first+1 >= last) return first;

    if (target > m->a[first].mass) {
        test = (first+last) / 2;
        if (target >= m->a[test].mass) {
            return search_moli1(target, m, test, last);
        } else {
            return search_moli1(target, m, first, test);
        }
    } else {
        if (first==0 || target < m->a[first-1].mass) return first;
    }
}


void search_weight_moli1(int idx, int iw, uint64_t orgsearch, uint64_t searchweight, uint64_t tolerance) {
    int aF[MAX_ELE];
    uint64_t i_mass;

    if (iw<0 || iw>=MAX_MASS) return;
    Moli1* m = mols[iw];
    if (!m) return;

    // search for first element larger org-tol
    uint64_t target = searchweight - tolerance;
    uint64_t start = search_moli1(target, m, 0, m->last);
    // run
    for (uint64_t i=start; i< m->last && m->a[i].mass < searchweight+tolerance; i++) {
        if (labs(m->a[i].mass - searchweight) < tolerance) {
            int2aF(aF, m->a[i].formel);
            for (int j=0; j<NBR_COUNTS; j++) {
                aF[j+COUNTS_OFFSET]= cur_mol[j];
                if (cur_mol[j] && mol_counts[j].ref) {
                    aF[mol_counts[j].iref] -= cur_mol[j];
                }
            }
            if (check_mol(aF)) {
                i_mass= mass_aF(aF);
                if (labs(i_mass-orgsearch)> 1100000) {
                    fprintf(stderr, "Diff zwischen %ld %ld (sw=%ld, iw=%d)\n", i_mass, orgsearch, searchweight, iw);
                } else {
                    push_result(idx, orgsearch, i_mass, aF);
                }
            }
        }
    }
}


void search_weight(int idx, uint64_t orgsearch, uint64_t searchweight, uint64_t tolerance) {
    int iw1, iw2;
    Moli1* m;

    iw1 = weight2int(searchweight - tolerance);
    iw2 = weight2int(searchweight + tolerance);

    if (iw1>0 && iw1<MAX_MASS) {
        m= mols[iw1];
        search_weight_moli1(idx, iw1, orgsearch, searchweight, tolerance);
    }
    if (iw2!=iw1 && iw2>0 && iw2<MAX_MASS) {
        m= mols[iw2];
        search_weight_moli1(idx, iw2, orgsearch, searchweight, tolerance);
    }
}


void calc_masses(uint64_t tolerance) {
    uint64_t tol;

    if (DO_DEBUG) fprintf(stderr, "Start calc_masses %ld\n", last_masses);
    qsort(masses, last_masses, sizeof(Masses), compareMasses);
    if (DO_DEBUG) fprintf(stderr, "    sorted\n");

    for (int i=0; i<last_masses; i++) {
        tol = masses[i].searchmass * tolerance / TOLDIV;
        pull_mass(i);
        read_filesfor(i, masses[i].mass, tol);
        search_weight(masses[i].idx, masses[i].searchmass, masses[i].mass, tol);
    }
    last_masses= 0;
}

void print_results() {
    uint64_t i_mass;
    uint64_t last_s= 0;
    int      id= 1;

    char formel[256], slist[256], mass[128], search[128], str[256];
    if (DO_DEBUG) fprintf(stderr, "Start print_results %ld\n", last_result);
    // we do nit give idx of hits instead idx of search list
    // qsort(results, last_result, sizeof(Result), compareResults);
    // if (DO_DEBUG) fprintf(stderr, "    sorted\n");

    for (int i=0; i<last_result; i++) {
        i_mass = mass_aF(results[i].a);
        mass2str(search, results[i].orgsearch);
        mass2str(mass, i_mass);
        aF2formel(formel, results[i].a);
        aF2list(slist, results[i].a);
        sprintf (str,"%d\t%s\t%s\t%f\t%f\t%f\t%s\t%s",
                     results[i].idx,
                     search, mass,
                     (double)labs(i_mass-results[i].orgsearch)/(double)results[i].orgsearch*TOLMUL, //DIFF
                     calc_hc(results[i].a), calc_oc(results[i].a),
                     slist, formel);
        puts(str);
    }
    last_result= 0;
}


int read_searchweights(char* fname) {
    int idx=0;
    double sv;

    FILE* infh = fopen(fname, "r");
    if (!infh) {
        printf("File %s nicht zu oeffnen", fname);
        exit(1);
    }
    while (fscanf(infh, "%lf", &sv)==1) {
        if (sv>0 && sv<2000) {
            searchweights[idx++]= (uint64_t)(sv * 1E9);
        } else {
            fprintf(stderr, "Ungueltige Masse Zeile %d", idx);
            exit(2);
        }
        if (idx>=MAX_SEARCH) {
            fprintf(stderr, "Too many Masses %d", idx);
            exit(2);
        }
    }
    searchweights[idx]=0;
    if (DO_DEBUG) {
        fprintf(stderr, "READ %d SEARCHES", idx);
    }
    return idx;
}

void metal_counts_to_zero() {
    for (int i=0; i<NBR_COUNTS; i++) {
        if (mol_counts[i].e.spec0 == 1) {
            mol_counts[i].cTo=0;
            if (DO_DEBUG) {
                fprintf(stderr, "set %d %s to zero\n", i, mol_counts[i].formel);
            }
        }
    }
}

void init_all() {
    for (int i=0; i<MAX_MASS; i++) {
        mols[i] = NULL;
    }
}


void main(int argc, char* *argv) {
    char*    outfilename;
    int      dospec= 1;
    int      doread= 0;
    int      doweight= 0;
    int      dointeractive= 0;
    int64_t  tolerance= 0;
    char     c;
    uint64_t dw=0;

    init_all();

    while ((c = getopt (argc, argv, "dirmf:w:o:t:")) != -1) {
        switch (c) {
            case 'd':
                DO_DEBUG = 1;
                break;
            case 't':
                tolerance = atoll(optarg);
                break;
            case 'w':
                if (strlen(optarg)<10 || strchr(optarg, '.'))
                    searchweights[doweight++] = (uint64_t)atof(optarg)*1E9;
                else
                    searchweights[doweight++] = atoll(optarg);
                searchweights[doweight]= 0;
                break;
            case 'f':
                doweight= read_searchweights(optarg);
                break;
            case 'r':
                doread = 1;
                break;
            case 'm':
                dospec= 0;
                break;
            case 'i':
                dointeractive = 1;
                break;
            case 'o':
                outfilename= optarg;
                break;
        }
    }
    if (doread) {

        for (int index = optind; index < argc; index++) {
            if (!file_exist(argv[index])) {
                printf ("Missing File %s, outfile=%s\n", argv[index], outfilename);
                exit(9);
            }
        }

        for (int index = optind; index < argc; index++) {
            printf("Read %s\n", argv[index]);
            read_file(argv[index]);
        }

        qsort(mols0, mols0_last, sizeof(Moli), compareMoli);
        write_binfile(outfilename);

    } else if (doweight) {

        // Initialize start
        init_alternatives();
        if (dospec==0)
            metal_counts_to_zero();
        print_table_header();

        for (int i=0; searchweights[i]; i++) {
            if (push_mass(i, searchweights[i], searchweights[i])) calc_masses(tolerance);
            while (next_mol()) {
                dw= mass_mol();
                if (dw < searchweights[i]) {
                    if (push_mass(i, searchweights[i], searchweights[i]-dw)) calc_masses(tolerance);
                }
            }
        }
        calc_masses(tolerance);
        print_results();
    } else if (dointeractive) {
        double sv;
        print_table_header();
        while (scanf("%lf", &sv)==1 && sv) {
            printf("%lf", sv);
            if (!doweight) {
                init_alternatives();
            }
            searchweights[doweight++]= (uint64_t)(sv * 1E9);
            if (push_mass(doweight-1, searchweights[doweight-1], searchweights[doweight-1])) calc_masses(tolerance);
            while (next_mol()) {
                dw= mass_mol();
                if (dw < searchweights[doweight-1]) {
                    if (push_mass(doweight-1, searchweights[doweight-1], searchweights[doweight-1]-dw)) calc_masses(tolerance);
                }
            }
        }
        calc_masses(tolerance);
        print_results();

    } else {

        puts(usage());
    }

    // test();
    exit(0);
}
//1450.014498200
