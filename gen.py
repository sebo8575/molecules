#!/usr/bin/python3
#
import os
import sys
from time import gmtime, strftime
import numpy as np

MAX_MASS = 2000
LINES_MOL = 2000
FILEPAT = "mol_%04d"
OUTDIR = 'masses/'

# description of elements
elements = (
    { "formel":'C'    , "weight": 12            , "min":1, "max":170, "val": 4, "spec": 0 },
    { "formel":'C13'  , "weight": 13.003354835  , "min":0, "max":  3, "val": 4, "spec": 2 },
    { "formel":'H'    , "weight": 1.0078250     , "min":1, "max":800, "val": 1, "spec": 0 },
    { "formel":'O'    , "weight": 15.99491461956, "min":0, "max": 91, "val": 2, "spec": 0 },
    { "formel":'O18'  , "weight": 17.9991610    , "min":0, "max":  1, "val": 2, "spec": 2 },
    { "formel":'N'    , "weight": 14.0030740048 , "min":0, "max": 51, "val": 5, "spec": 0 },
    { "formel":'N15'  , "weight": 15.000108899  , "min":0, "max":  1, "val": 5, "spec": 2 },
    { "formel":'S'    , "weight": 31.972072     , "min":0, "max": 31, "val": 6, "spec": 0 },
    { "formel":'S34'  , "weight": 33.96786690   , "min":0, "max":  1, "val": 6, "spec": 2 },
    { "formel":'P'    , "weight": 30.973763     , "min":0, "max": 20, "val": 5, "spec": 0 },
    { "formel":'Cl'   , "weight": 34.9688527    , "min":0, "max":  2, "val": 1, "spec": 1 },
    { "formel":'Cui'  , "weight": 62.92959772   , "min":0, "max":  2, "val": 1, "spec": 1 },
    { "formel":'Cuii' , "weight": 62.92959772   , "min":0, "max":  2, "val": 2, "spec": 1 },
    { "formel":'Feii' , "weight": 55.934936     , "min":0, "max":  2, "val": 2, "spec": 1 },
    { "formel":'FEiii', "weight": 55.934936     , "min":0, "max":  2, "val": 3, "spec": 1 },
    { "formel":'BR79' , "weight": 78.918338     , "min":0, "max":  2, "val": 1, "spec": 1 },
    { "formel":'BR81' , "weight": 80.91690      , "min":0, "max":  2, "val": 1, "spec": 1 },
    { "formel":'Na'   , "weight": 22.98976928   , "min":0, "max":  2, "val": 1, "spec": 1 },
    { "formel":'Ni.ii', "weight": 57.9353462    , "min":0, "max":  2, "val": 2, "spec": 1 },
    { "formel":'Znii' , "weight": 65.38         , "min":0, "max":  2, "val": 2, "spec": 1 },
    { "formel":'I'    , "weight": 126.90447     , "min":0, "max":  2, "val": 1, "spec": 1 },
    )

LE= len(elements)

# create outdir if not exists
if not os.path.isdir(OUTDIR):
    os.mkdir(OUTDIR)

# from formel to idx
idx= {}
for i in range(len(elements)):
    idx[elements[i]['formel']] = i

for i in range(len(elements)):
    # if elements[i]['max']>10:
        # elements[i]['max'] = 10
    if elements[i]['spec'] > 0:
        elements[i]['max'] = 0
elements[idx['P']]['min'] = 0 # replace by
elements[idx['P']]['max'] = elements[idx['P']]['min']

# prepare some arrays
moltype = np.dtype([('weight', np.float64), ('name', np.str_, 512)])
mols= np.ndarray(shape=(MAX_MASS,LINES_MOL), dtype=moltype)
posi= [0] * MAX_MASS


def info(s):
    print("%s       %s" % (strftime("%H:%M:%S", gmtime()), s))
    sys.stdout.flush()


def check_strct(s,w):
    """ Check for relations between element, total mass, bla bla,
        return  True for seemingly valid element,
                False otherwise
    """
    if w > 2000:
        return False
    # Valence check
    val = 0
    specs = 0
    for (i,e) in enumerate(elements):
        val += s[i] * e['val']
        if e['spec'] == 1:
            specs += s[i]

    if val % 2 == 1:
        return False

    if specs > 2:
        return False

    totC= (s[idx['C']] + s[idx['C13']])
    totS= (s[idx['S']] + s[idx['S34']])
    totO= (s[idx['O']] + s[idx['O18']])
    totN= (s[idx['N']] + s[idx['N15']])

    db = 1 + 0.5 * (totC * 2 - s[idx['H']] - s[idx['Cl']] + totN + s[idx['P']])
    if db<0:
        return False

    if totO / totC >2.1:
        return False

    if s[idx['H']] / totC > 8:
        return False

    if totS > totC:
        return False

    if s[idx['P']] > totC:
        return False

    if totN > 1.25 * totC:
        return False

    return True


def format_strct(s):
    """ Formats molecule in a nice way """
    m=''
    for (i,e) in enumerate(elements):
        if s[i]>0:
            m += e['formel'] + '_' + str(s[i]) + ' '
    return m

def calc_weight(s):
    """ return total weight of molekule descripte by s """
    w=0
    for (i,e) in enumerate(elements):
        w= w + s[i] * e['weight']
    return w

def flush_mass(w):
    """ writes out masses for group w, w is integer for masses m
        with w<= m < w+1
        in case there is something """
    if posi[w]>0:
        filename= FILEPAT % w
        # print("Filename %s, cnt=%d" % (filename,posi[w]))
        s = [ "%16.10f\t%s\n" % (mols[w][l][0], mols[w][l][1]) for l in range(posi[w]) ]
        with open(os.path.join(OUTDIR, filename), 'a') as f:
            f.write("".join(s))
        posi[w]=0


if __name__ == "__main__":
    # set start values
    strct= [ e['min'] for e in elements ]

    # start the work
    info("Started")
    while True:
        w= calc_weight(strct)
        if check_strct(strct, w):
            # we have a valid molecule
            widx= int(w)
            mols[widx][posi[widx]]= (w, format_strct(strct))
            posi[widx]+= 1
            if posi[widx]>=LINES_MOL:
                flush_mass(widx)

        # build next molecule
        inc_done = False
        for (i,e) in enumerate(elements):
            if strct[i]>=e['max']:
                # hit top, -> fall down and continue to increment next position
                strct[i]=e['min']
            else:
                # increment next position and done
                strct[i]= strct[i]+1
                inc_done =True
                if i>6:
                    info("increment %s to %d" % (e['formel'], strct[i]))
                break
        # no increment - so we had had reached the last element
        if not inc_done:
            break

    # flush the rest ....
    for l in range(MAX_MASS):
        flush_mass(l)

    info("Done")
