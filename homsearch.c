#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <getopt.h>
#include <math.h>
#include <ctype.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/types.h>

#define LINE_SIZE 300

typedef struct {
    int id; // index of formula
    int C, H, O;
    char rest[32];
    int group;
    int rgroup;
} MOLS;
MOLS* mols;
int lastmols = 0,
	DO_DEBUG = 0;
int hom_ch2 = 0,
    hom_o = 0,
    hom_h2 = 0,
    hom_co2 = 0,
    hom_h2o = 0;


int cmpMOLS (const void * a, const void * b) {
	MOLS *ma, *mb;
	int r = 0;
	ma = (MOLS *)a;
	mb = (MOLS *)b;
	r = strcmp(ma->rest, mb->rest);
	if (r!=0) return r;
	if (ma->C < mb->C) return -1;
	if (ma->C > mb->C) return 1;
	if (ma->H < mb->H) return -1;
	if (ma->H > mb->H) return 1;
	if (ma->O < mb->O) return -1;
	if (ma->O > mb->O) return 1;
	return 0;
}

void read_homsearch_csv(char* fname) {
    char line[LINE_SIZE], buf[32];
    int line_count, bufcnt, colidx = 0,
        colC=-1, colH=-1, colO=-1, colMass=-1;

    FILE* infh = fopen(fname, "r");
    if (!infh) {
        printf("homsearch-File %s nicht zu oeffnen\n", fname);
        exit(1);
    }
    line_count= 0;
    while (fgets(line, LINE_SIZE, infh) != NULL)  {
        // printf("<%s>", line);
        line_count += 1;
    }
    fprintf(stderr, "linecount=%d\n", line_count);
    mols = (MOLS *)malloc(line_count * sizeof(MOLS));
    rewind(infh);
    buf[0]= '\0'; bufcnt = 0;
    if (!fgets(line, LINE_SIZE, infh)) {
    	exit(9);
    }
    for (char* c = line; *c; c++) {
        if (*c==',' || *c=='\n') {
            buf[bufcnt++] = '\0';
            if (strcmp(buf, "C") == 0) {
                colC = colidx;
            } else if (strcmp(buf, "H") == 0) {
                colH = colidx;
            } else if (strcmp(buf, "O") == 0) {
                colO = colidx;
            }

            buf[0]='\0'; bufcnt = 0; colidx += 1;
        } else {
            buf[bufcnt++] = *c;
        }
    }
    colMass = colidx - 1;
    fprintf(stderr, "C:%d\nH:%d\nO:%d\nMASS:%d\n", colC, colH, colO, colMass);

    int i = 0;
    char* end;
    while (fgets(line, LINE_SIZE, infh) != NULL)  {
        colidx = 0;
        mols[i].rest[0] = '\0';
        for (char* c = line; *c; c++) {
            if (*c==',' || *c=='\n') {
                buf[bufcnt++] = '\0';
                if (colidx == 0) {
                    mols[i].id = strtol(buf, NULL, 10);
                } else if (colidx == colC) {
                    mols[i].C = strtol(buf, NULL, 10);
                } else if (colidx == colH) {
                    mols[i].H = strtol(buf, NULL, 10);
                } else if (colidx == colO) {
                    mols[i].O = (int)strtol(buf, NULL, 10);
                } else if (colidx == colMass) {
                } else {
                    strcat(mols[i].rest, buf);
                    strcat(mols[i].rest, ",");
                }

                buf[0]='\0'; bufcnt = 0; colidx += 1;
            } else {
                buf[bufcnt++] = *c;
            }
        }
        lastmols = i;
        i+=1;
    }
    fclose(infh);

    qsort(mols, lastmols, sizeof(MOLS), cmpMOLS);

    int rgroup = 0;
    for (int i=0; i<lastmols; i++) {
    	if (strcmp(line, mols[i].rest)) {
    		rgroup += 1;
    		strcpy(line, mols[i].rest);
    	}
    	mols[i].rgroup = rgroup;
    	mols[i].group = 0;
	}
    // for (int i=0; i<lastmols; i++) {
    //     printf("id=%8d C:%3d H:%3d O:%3d rest:%s (%d)\n", mols[i].id, mols[i].C, mols[i].H, mols[i].O, mols[i].rest, mols[i].rgroup);
    // }
}


int homcheck(MOLS *ma, MOLS *mb) {
	if (ma->O == mb->O && ma->C == mb->C && ma->H == mb->H) return 9;
	// CH2
	if (hom_ch2 && ma->O == mb->O && ma->C+1 == mb->C && ma->H+2 == mb->H) return 1;
	// O
	if (hom_o && ma->C == mb->C && ma->H == mb->H && ma->O+1 == mb->O) return 2;
	// H2
	if (hom_h2 && ma->C == mb->C && ma->O == mb->O && ma->H+2 == mb->H) return 3;
	// CO2
	if (hom_co2 && ma->H == mb->H && ma->C+1 == mb->C && ma->O+2 == mb->O) return 4;
	// h2O
	if (hom_h2o && ma->C == mb->C && ma->H+2 == mb->H && ma->O+1 == mb->O) return 5;

	return 0;
}


void overwritegroup(int old, int new) {
	if (old==new) return;
	// printf("ov %d > %d\n", old, new);
	for (int i=0; i<lastmols; i++) {
		if (mols[i].group == old) {
			mols[i].group = new;
		}
	}
}

void do_homsearch() {
	int agroup;
	int newhomgroup = 0;
	int homgroup;
	for (int idx=0; idx<lastmols; idx++) {
		agroup = mols[idx].rgroup;

		if (mols[idx].group == 0) {
			newhomgroup += 1;
			mols[idx].group = newhomgroup;
		}
		homgroup = mols[idx].group;

		for (int bidx=idx+1; bidx<lastmols && mols[bidx].rgroup == agroup; bidx++) {
			// CH2
			if (homcheck(&(mols[idx]), &(mols[bidx]))) {
				if (mols[bidx].group == 0) {
					mols[bidx].group = homgroup;
				} else {
					overwritegroup(mols[bidx].group, homgroup);
				}
			}
		}

	}
}

void main(int argc, char* *argv) {
	char homfilename[128];
	char c;

	while ((c = getopt (argc, argv, "hd12345")) != -1) {
        switch (c) {
            case 'd':
                DO_DEBUG = 1;
                break;
            case '1':
                hom_ch2 = 1;
                break;
            case '2':
                hom_o = 1;
                break;
            case '3':
                hom_h2 = 1;
                break;
            case '4':
                hom_co2 = 1;
                break;
            case '5':
                hom_h2o = 1;
                break;
        }
    }
    fprintf(stderr, "DO Ch2 %d, O %d, H2 %d, Co2 %d, h2o %d\n", hom_ch2, hom_o, hom_h2, hom_co2, hom_h2o);

	if (optind < argc) {
		strcpy(homfilename, argv[optind]);
	} else {
		strcpy(homfilename, "homsearch.csv");
	}
    read_homsearch_csv(homfilename);
    do_homsearch();
    for (int i=0; i<lastmols; i++) {
        // fprintf(stderr, "id=%8d C:%3d H:%3d O:%3d rest:%s (%d) G%d\n", mols[i].id, mols[i].C, mols[i].H, mols[i].O, mols[i].rest, mols[i].rgroup, mols[i].group);
        printf("%d,%d\n", mols[i].id, mols[i].group);
    }
    free(mols);
}
