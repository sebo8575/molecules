#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include <sys/stat.h>

#define MAX_MASS 2000
#define MAX_LINES 2000

typedef long double MFLOAT;

typedef struct {
    int64_t  mass;
    char     formel[256];
} Mol;

typedef struct {
   char    formel[10];
   int64_t mass;
   int     valence;
   int     spec;
} Element;

typedef struct {
    char    formel[10];
    int     cFrom;
    int     cTo;
    int     cur;
    Element e;
} Count;

Mol res[MAX_MASS][MAX_LINES];
int residx[MAX_MASS];
int argP;

Element elements[] = {
    { "C"    ,  12000000000  ,  4, 0 },
    { "C13"  ,  13003354835  ,  4, 2 },
    { "H"    ,   1007825000  ,  1, 0 },
    { "O"    ,  15994914619  ,  2, 0 },
    { "O18"  ,  17999161000  ,  2, 2 },
    { "N"    ,  14003074004  ,  5, 0 },
    { "N15"  ,  15000108899  ,  5, 2 },
    { "S"    ,  31972072000  ,  6, 0 },
    { "S34"  ,  33967866900  ,  6, 2 },
    { "P"    ,  30973763000  ,  5, 0 },
    { "Cl"   ,  34968852700  ,  1, 1 },
    { "Cui"  ,  62929597720  ,  1, 1 },
    { "Cuii" ,  62929597720  ,  2, 1 },
    { "Feii" ,  55934936000  ,  2, 1 },
    { "FEiii",  55934936000  ,  3, 1 },
    { "BR79" ,  78918338000  ,  1, 1 },
    { "BR81" ,  80916900000  ,  1, 1 },
    { "Na"   ,  22989769280  ,  1, 1 },
    { "Ni.ii",  57935346200  ,  2, 1 },
    { "Znii" ,  65380000000  ,  2, 1 },
    { "I"    , 126904470000  ,  1, 1 } };
int nbr_elements= sizeof(elements)/sizeof(Element);

Count counts[] = {
    { "C"    ,1, 170 },
    { "H"    ,1, 800 },
    { "O"    ,0,  91 },
    { "N"    ,0,  51 },
    { "S"    ,0,  31 },
    { "P"    ,0,   0 }, // 0..20
    { "Cl"   ,0,   2 },
};
int nbr_counts= sizeof(counts)/sizeof(Count);
int argP=0;

int64_t lines_written = 0;


double flush_mass(int midx) {
    char filename[256];
    FILE *fp;
    if (residx[midx]) {

        sprintf(filename, "masses_%02dP/m_%04d.txt", argP, midx);
        fp = fopen(filename, "a");

        if(fp == NULL) {
            printf("Datei %s konnte NICHT geoeffnet werden.\n", filename);
            exit(9);
        }

        for (int i = 0; i < residx[midx]; ++i) {
            fprintf(fp, "%15ld\t%s\n", res[midx][i].mass, res[midx][i].formel);
        }

        fclose(fp);

        lines_written += residx[midx];
        residx[midx] = 0;
    }
}


double mass_mol() {
    int64_t  m= 0;
    for (int i = 0; i < nbr_counts; ++i) {
        if (counts[i].cur>0) {
            m = m + counts[i].cur*counts[i].e.mass;
        }
    }
    return m;
}


char* format_mol(char* f) {
    f[0] = '\0';
    for (int i = 0; i < nbr_counts; ++i) {
        if (counts[i].cur>0) {
            sprintf(f+strlen(f), "%s_%d ", counts[i].e.formel, counts[i].cur);
        }
    }
    return f;
}


Mol* check_and_store_mol() {
    double
        cC=0,
        cH=0,
        cO=0,
        cN=0,
        cS=0,
        cP=0,
        cCl=0,
        cNa=0,
        db;

    double mass;
    int massidx;
    Mol* mol;

    for (int i = 0; i < nbr_counts; ++i) {
        if (0 == strcmp(counts[i].e.formel, "C"))       cC=  (double)counts[i].cur;
        else if (0 == strcmp(counts[i].e.formel, "H"))  cH=  (double)counts[i].cur;
        else if (0 == strcmp(counts[i].e.formel, "O"))  cO=  (double)counts[i].cur;
        else if (0 == strcmp(counts[i].e.formel, "N"))  cN=  (double)counts[i].cur;
        else if (0 == strcmp(counts[i].e.formel, "S"))  cS=  (double)counts[i].cur;
        else if (0 == strcmp(counts[i].e.formel, "P"))  cP=  (double)counts[i].cur;
        else if (0 == strcmp(counts[i].e.formel, "Cl")) cCl= (double)counts[i].cur;
        else if (0 == strcmp(counts[i].e.formel, "Na")) cNa= (double)counts[i].cur;
    }

    db= 1 + 0.5 * (cC *2 - cH - cCl + cN + cP);
    if (db < 0) return 0;

    if (cO / cC > 2.1) return 0;
    if (cH / cC > 8) return 0;
    if (cS > cC) return 0;
    if (cP > cC) return 0;
    if (cN > 1.25 * cC) return 0;

    mass= mass_mol();
    if (mass>(int64_t)MAX_MASS*1000000000LL) return 0;

    massidx= mass/1000000000LL;

    mol = &(res[massidx][residx[massidx]]);
    mol->mass= mass;
    format_mol(mol->formel);

    residx[massidx] += 1;

    if (residx[massidx]>=MAX_LINES) {
        flush_mass(massidx);
    }

    return mol;
}


int next_mol() {
    int inc_done = 0;
    for (int i = 0; i < nbr_counts; ++i) {
        if (counts[i].cur<counts[i].cTo) {
            counts[i].cur += 1;
            return 1;
        } else {
            counts[i].cur = counts[i].cFrom;
        }
    }
    return 0;
}


void main(int argc, char* *argv) {
    int64_t cnt_mol=0;
    Mol* mol;
    if (argc==2) {
        argP= atoi(argv[1]);
        counts[5].cFrom= argP;
        counts[5].cTo= argP;
    } else {
        argP= 0;
    }

    printf("Create Mols with P_%d\n", argP);

    char dirname[32];
    struct stat sb;
    sprintf(dirname, "masses_%02dP", argP);
    if (stat(dirname, &sb) != 0) {
        mkdir(dirname,0777);
    }


    for (int i = 0; i < MAX_MASS; ++i) {
        residx[i] = 0;
    }

    // initialize start mol and copy elements
    for (int i = 0; i < nbr_counts; ++i) {
        counts[i].cur= counts[i].cFrom;
        for (int j = 0; j < nbr_elements; ++j) {
            if (strcmp(counts[i].formel, elements[j].formel) == 0) {
                memcpy(&(counts[i].e), &(elements[j]), sizeof(Element));
                counts[i].e= elements[j];
                break;
            }
        }
    }

    while (1) {
        if (mol = check_and_store_mol()) {
            cnt_mol += 1;
            if (cnt_mol % 100000 == 0) {
                printf(". ");
                if (cnt_mol % 5000000 == 0) {
                    printf("%ld %s\n", cnt_mol, mol->formel);
                    fflush(stdout);
                }
            }
            // printf("%f %s\n", mass_mol(), format_mol());
        }
        if (!next_mol()) break;
    }
    for (int i = 0; i < MAX_MASS; ++i) {
        flush_mass(i);
    }

    printf("\n\ngenerated %ld molecules,\nwritten   %ld\n", cnt_mol, lines_written);
}
